function openNav() {
    document.getElementById("mySidenav").style.width = "300px";
    document.getElementById("main").style.marginLeft = "auto";
    document.getElementById("main").style.setProperty('marginLeft', 'calc(100% - 300px)');
    document.getElementById("openNav").style.display = "none";
    
    let w = document.body.clientWidth;
    let main_w = document.getElementById("main").clientWidth;
    if ( ( w - 800 ) * 0.5 <= 300 ) {
      document.getElementById("main").style.marginRight = ( w - 800 ) * 0.5 + "px"
      document.getElementById("top-bar").style.marginRight = ( w - 800 ) * 0.5 + 32 + "px"
      document.getElementById("main").style.marginLeft = 300 + "px"
    }
    document.getElementById("top-bar").style.setProperty('width', document.getElementById("main").clientWidth + 'px');
  }
  
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("openNav").style.display = "block";
    document.getElementById("main").style.margin = "auto";
    document.getElementById("main").style.marginTop = "128px";
    document.getElementById("top-bar").style.paddingRight = "32px"
    
    document.getElementById("top-bar").style.setProperty('width', '800px');
    document.getElementById("top-bar").style.setProperty('maxWidth', '800px');
    document.getElementById("top-bar").style.setProperty('margin', 'auto');
    document.getElementById("top-bar").style.setProperty('marginRight', '64px');

    document.getElementById("top-bar").style.setProperty('width', document.getElementById("main").clientWidth + 'px');
  }
  
  window.addEventListener('resize', function(event) {
    document.getElementById("top-bar").style.setProperty('width', document.getElementById("ma in").clientWidth + 'px');
  }, true);
  
  const copyButtonLabel = "kód másolása";
  
  // use a class selector if available
  let blocks = document.querySelectorAll("pre");
  
  blocks.forEach((block) => {
    // only add button if browser supports Clipboard API
    if (navigator.clipboard) {
      let button = document.createElement("button");
      
      button.innerText = copyButtonLabel;
      block.appendChild(button);
      
      button.addEventListener("click", async () => {
        await copyCode(block, button);
      });
    }
  });
  
  async function copyCode(block, button) {
    let code = block.querySelector("code");
    let text = code.innerText;
    
    await navigator.clipboard.writeText(text);
    
    // visual feedback that task is completed
    button.innerText = "sikeresen másolva";
    
    setTimeout(() => {
      button.innerText = copyButtonLabel;
    }, 700);
  }